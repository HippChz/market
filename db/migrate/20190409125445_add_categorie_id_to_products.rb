class AddCategorieIdToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :categorie_id, :string
    add_index :products, :categorie_id
  end
end
