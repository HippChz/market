class CreatePromotions < ActiveRecord::Migration[5.2]
  def change
    create_table :promotions do |t|
      t.float :percentage
      t.datetime :start
      t.datetime :end

      t.timestamps
    end
  end
end
