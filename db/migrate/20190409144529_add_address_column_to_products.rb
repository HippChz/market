class AddAddressColumnToProducts < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :address
    add_column :products, :street, :string
    add_column :products, :city, :string
    add_column :products, :state, :string
    add_column :products, :country, :string
  end
end
