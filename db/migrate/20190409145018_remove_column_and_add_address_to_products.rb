class RemoveColumnAndAddAddressToProducts < ActiveRecord::Migration[5.2]
  def change
    add_column :products, :address, :string
    remove_column :products, :street
    remove_column :products, :city
    remove_column :products, :state
    remove_column :products, :country
  end
end
