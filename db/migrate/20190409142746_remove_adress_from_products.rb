class RemoveAdressFromProducts < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :adress, :string
  end
end
