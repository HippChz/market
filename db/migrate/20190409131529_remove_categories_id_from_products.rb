class RemoveCategoriesIdFromProducts < ActiveRecord::Migration[5.2]
  def change
    remove_column :products, :categorie_id, :string
  end
end
