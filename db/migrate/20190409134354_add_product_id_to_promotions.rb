class AddProductIdToPromotions < ActiveRecord::Migration[5.2]
  def change
    add_column :promotions, :product_id, :integer
  end
end
