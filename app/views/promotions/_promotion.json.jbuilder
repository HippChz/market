json.extract! promotion, :id, :percentage, :start, :end, :created_at, :updated_at
json.url promotion_url(promotion, format: :json)
