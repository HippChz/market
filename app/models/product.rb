class Product < ApplicationRecord
    belongs_to :category
    geocoded_by :address
    after_validation :geocode
end
